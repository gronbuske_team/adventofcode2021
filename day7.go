package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strings"
)

func crabDistance(crabs []int, point int) int {
	result := 0
	for c := range crabs {
		result += int(math.Abs(float64(crabs[c]) - float64(point)))
	}
	return result
}

func realCrabDistance(crabs []int, point int) int {
	result := 0
	for c := range crabs {
		dist := int(math.Abs(float64(crabs[c]) - float64(point)))
		for x := 1; x <= dist; x++{
			result += x
		}
	}
	return result
}

func day7a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	crabs := []int{}
	lowest := 0
	highest := 0
	result := 0
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), ",")
		for n := range s {
			t := getNum(s[n])
			crabs = append(crabs, t)
			highest = int(math.Max(float64(highest), float64(t)))
		}
		result = crabDistance(crabs, highest)
		for i := lowest; i < highest; i++ {
			temp := crabDistance(crabs, i)
			if temp < result {
				result = temp
			}
		}
		return result
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

func day7b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	crabs := []int{}
	lowest := 0
	highest := 0
	result := 0
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), ",")
		for n := range s {
			t := getNum(s[n])
			crabs = append(crabs, t)
			highest = int(math.Max(float64(highest), float64(t)))
		}
		result = realCrabDistance(crabs, highest)
		for i := lowest; i < highest; i++ {
			temp := realCrabDistance(crabs, i)
			if temp < result {
				result = temp
			}
		}
		return result
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

func day7() {

	testresult := day7a("./day7.test")
	expected := 37
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day7a("./day7.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day7b("./day7.test")
	expected = 168
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day7b("./day7.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
)

func day3a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)

	ones := []int{}
	zeroes := []int{}
	gamma := float64(0)
	epsilon := float64(0)

	for scanner.Scan() {
		for pos, char := range scanner.Text() {
			if pos >= len(ones) {
				ones = append(ones, 0)
				zeroes = append(zeroes, 0)
			}
			if char == '1' {
				ones[pos] += 1
			}
			if char == '0' {
				zeroes[pos] += 1
			}
		}
	}
	for i := 0; i < len(ones); i++ {
		if ones[len(ones)-i-1] > zeroes[len(ones)-i-1] {
			gamma += math.Pow(2, float64(i))
		} else {
			epsilon += math.Pow(2, float64(i))
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return int(gamma) * int(epsilon)
}

func day3b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	ones := []int{}
	zeroes := []int{}
	oxygen := float64(0)
	scrubber := float64(0)
	valid_oxygen := []string{}
	valid_scrubber := []string{}

	for scanner.Scan() {
		valid_oxygen = append(valid_oxygen, scanner.Text())
		valid_scrubber = append(valid_scrubber, scanner.Text())
		for pos, char := range scanner.Text() {
			if pos >= len(ones) {
				ones = append(ones, 0)
				zeroes = append(zeroes, 0)
			}
			if char == '1' {
				ones[pos] += 1
			}
			if char == '0' {
				zeroes[pos] += 1
			}
		}
	}
	ones_scrubber := make([]int, len(ones))
	zeroes_scrubber := make([]int, len(ones))
	copy(ones_scrubber, ones)
	copy(zeroes_scrubber, zeroes)
	for i := 0; i < len(ones); i++ {
		if len(valid_oxygen) > 1 {
			temp_oxygen := []string{}
			if ones[i] >= zeroes[i] {
				for _, s := range valid_oxygen {
					if s[i] == '1' {
						temp_oxygen = append(temp_oxygen, s)
					} else {
						for j := i + 1; j < len(ones); j++ {
							if s[j] == '1' {
								ones[j] -= 1
							} else {
								zeroes[j] -= 1
							}
						}
					}
				}
			} else {
				for _, s := range valid_oxygen {
					if s[i] == '0' {
						temp_oxygen = append(temp_oxygen, s)
					} else {
						for j := i + 1; j < len(ones); j++ {
							if s[j] == '1' {
								ones[j] -= 1
							} else {
								zeroes[j] -= 1
							}
						}
					}
				}
			}
			valid_oxygen = temp_oxygen
		}
		if len(valid_scrubber) > 1 {
			temp_scrubber := []string{}
			if ones_scrubber[i] >= zeroes_scrubber[i] {
				for _, s := range valid_scrubber {
					if s[i] == '0' {
						temp_scrubber = append(temp_scrubber, s)
					} else {
						for j := i + 1; j < len(ones); j++ {
							if s[j] == '1' {
								ones_scrubber[j] -= 1
							} else {
								zeroes_scrubber[j] -= 1
							}
						}
					}
				}
			} else {
				for _, s := range valid_scrubber {
					if s[i] == '1' {
						temp_scrubber = append(temp_scrubber, s)
					} else {
						for j := i + 1; j < len(ones); j++ {
							if s[j] == '1' {
								ones_scrubber[j] -= 1
							} else {
								zeroes_scrubber[j] -= 1
							}
						}
					}
				}
			}
			valid_scrubber = temp_scrubber
		}
	}
	for i := 0; i < len(valid_oxygen[0]); i++ {
		if valid_oxygen[0][len(valid_oxygen[0])-i-1] == '1' {
			oxygen += math.Pow(2, float64(i))
		}
		if valid_scrubber[0][len(valid_scrubber[0])-i-1] == '1' {
			scrubber += math.Pow(2, float64(i))
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return int(oxygen) * int(scrubber)
}

func day3() {

	testresult := day3a("./day3.test")
	expected := 198
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day3a("./day3.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day3b("./day3.test")
	expected = 230
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day3b("./day3.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

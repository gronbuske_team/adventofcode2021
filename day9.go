package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func day9a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	result := 0
	grid := [][]int{}
	c := 0
	for scanner.Scan() {
		text := scanner.Text()
		grid = append(grid, []int{})
		for s := range text {
			grid[c] = append(grid[c], getNum(string(text[s])))
		}
		c++
	}
	for x := range grid {
		for y := range grid[x] {
			smaller := true
			if x != 0 && grid[x-1][y] <= grid[x][y] {
				smaller = false
			}
			if x != len(grid)-1 && grid[x+1][y] <= grid[x][y] {
				smaller = false
			}
			if y != 0 && grid[x][y-1] <= grid[x][y] {
				smaller = false
			}
			if y != len(grid[x])-1 && grid[x][y+1] <= grid[x][y] {
				smaller = false
			}
			if smaller {
				result += grid[x][y] + 1
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

var basinGrid [][]int

func calculateBasin(x int, y int) bool {
	if x < 0 || x >= len(basinGrid) {
		return false
	}
	if y < 0 || y >= len(basinGrid[x]) {
		return false
	}
	if basinGrid[x][y] == 9 || basinGrid[x][y] == 0 {
		return false
	}

	smaller := true
	if x != 0 && basinGrid[x-1][y] < basinGrid[x][y] && basinGrid[x-1][y] != 0 {
		smaller = false
	}
	if x != len(basinGrid)-1 && basinGrid[x+1][y] < basinGrid[x][y] && basinGrid[x+1][y] != 0 {
		smaller = false
	}
	if y != 0 && basinGrid[x][y-1] < basinGrid[x][y] && basinGrid[x][y-1] != 0 {
		smaller = false
	}
	if y != len(basinGrid[x])-1 && basinGrid[x][y+1] < basinGrid[x][y] && basinGrid[x][y+1] != 0 {
		smaller = false
	}
	return smaller
}

func checkAround(x int, y int) int {
	result := 0
	for {
		done := true
		if calculateBasin(x-1, y) {
			basinGrid[x-1][y] = 0
			result += checkAround(x-1, y)
			done = false
		}
		if calculateBasin(x+1, y) {
			basinGrid[x+1][y] = 0
			result += checkAround(x+1, y)
			done = false
		}
		if calculateBasin(x, y-1) {
			basinGrid[x][y-1] = 0
			result += checkAround(x, y-1)
			done = false
		}
		if calculateBasin(x, y+1) {
			basinGrid[x][y+1] = 0
			result += checkAround(x, y+1)
			done = false
		}
		if done {
			return result + 1
		}
	}
}

func day9b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	result1 := 0
	result2 := 0
	result3 := 0
	basinGrid = [][]int{}
	c := 0
	for scanner.Scan() {
		text := scanner.Text()
		basinGrid = append(basinGrid, []int{})
		for s := range text {
			basinGrid[c] = append(basinGrid[c], getNum(string(text[s])))
		}
		c++
	}
	for x := range basinGrid {
		for y := range basinGrid[x] {
			smaller := true
			if x != 0 && basinGrid[x-1][y] <= basinGrid[x][y] {
				smaller = false
			}
			if x != len(basinGrid)-1 && basinGrid[x+1][y] <= basinGrid[x][y] {
				smaller = false
			}
			if y != 0 && basinGrid[x][y-1] <= basinGrid[x][y] {
				smaller = false
			}
			if y != len(basinGrid[x])-1 && basinGrid[x][y+1] <= basinGrid[x][y] {
				smaller = false
			}
			if smaller {
				basinGrid[x][y] = 0
				size := checkAround(x, y)
				if size >= result1 {
					result3 = result2
					result2 = result1
					result1 = size
				} else if size >= result2 {
					result3 = result2
					result2 = size
				} else if size >= result3 {
					result3 = size
				}
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result1 * result2 * result3
}

func day9() {

	testresult := day9a("./day9.test")
	expected := 15
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day9a("./day9.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day9b("./day9.test")
	expected = 1134
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day9b("./day9.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

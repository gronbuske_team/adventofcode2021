package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func onlyTrue(list []bool) bool {
	for _, a := range list {
		if !a {
			return false
		}
	}
	return true
}

func check_board(numbers []int, board [5][5]int) [2]int {
	check := [5][5]bool{}
	for n := range numbers {
		for x := 0; x < 5; x++ {
			for y := 0; y < 5; y++ {
				if board[x][y] == numbers[n] {
					check[x][y] = true
					temp := []bool{}
					temp = append(temp, check[0][y], check[1][y], check[2][y], check[3][y], check[4][y])
					if onlyTrue(check[x][:]) || onlyTrue(temp) {
						score := 0
						for x2 := 0; x2 < 5; x2++ {
							for y2 := 0; y2 < 5; y2++ {
								if !check[x2][y2] {
									score += board[x2][y2]
								}
							}
						}
						score *= numbers[n]
						return [2]int{n, score}
					}
				}
			}
		}
	}
	return [2]int{0, 0}
}

func day4a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	current_board := [5][5]int{}
	numbers := []int{}
	current_row := 0
	winner := 25
	winning_nr := 0
	for scanner.Scan() {
		text := strings.Replace(scanner.Text(), ",", " ", -1)
		s := strings.Split(text, " ")
		if len(numbers) == 0 {
			for i := 0; i < len(s); i++ {
				n, e := strconv.Atoi(s[i])
				if e != nil {
					fmt.Println(e)
					os.Exit(2)
				}
				numbers = append(numbers, n)
			}
			scanner.Scan()
		} else if len(s) <= 1 {
			result := check_board(numbers, current_board)
			if winner >= result[0] {
				winner = result[0]
				winning_nr = result[1]
			}
			current_board = [5][5]int{}
			current_row = 0
		} else {
			c := 0
			for i := 0; i < len(s); i++ {
				if s[i] != "" {
					n, e := strconv.Atoi(s[i])
					if e != nil {
						fmt.Println(e)
						os.Exit(2)
					}
					current_board[current_row][c] = n
					c++
				}
			}
			current_row += 1
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	result := check_board(numbers, current_board)
	if winner >= result[0] {
		winning_nr = result[1]
	}
	return winning_nr
}

func day4b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	current_board := [5][5]int{}
	numbers := []int{}
	current_row := 0
	loser := 0
	losing_nr := 0
	for scanner.Scan() {
		text := strings.Replace(scanner.Text(), ",", " ", -1)
		s := strings.Split(text, " ")
		if len(numbers) == 0 {
			for i := 0; i < len(s); i++ {
				n, e := strconv.Atoi(s[i])
				if e != nil {
					fmt.Println(e)
					os.Exit(2)
				}
				numbers = append(numbers, n)
			}
			scanner.Scan()
		} else if len(s) <= 1 {
			result := check_board(numbers, current_board)
			if loser <= result[0] {
				loser = result[0]
				losing_nr = result[1]
			}
			current_board = [5][5]int{}
			current_row = 0
		} else {
			c := 0
			for i := 0; i < len(s); i++ {
				if s[i] != "" {
					n, e := strconv.Atoi(s[i])
					if e != nil {
						fmt.Println(e)
						os.Exit(2)
					}
					current_board[current_row][c] = n
					c++
				}
			}
			current_row += 1
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	result := check_board(numbers, current_board)
	if loser <= result[0] {
		losing_nr = result[1]
	}
	return losing_nr
}

func day4() {

	testresult := day4a("./day4.test")
	expected := 4512
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day4a("./day4.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day4b("./day4.test")
	expected = 1924
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day4b("./day4.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

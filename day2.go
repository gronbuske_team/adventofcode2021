package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func day2b(inputfile string) int {
	// open file
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	// remember to close the file at the end of the program
	defer f.Close()

	// read the file line by line using scanner
	scanner := bufio.NewScanner(f)

	aim := 0
	xpos := 0
	depth := 0

	for scanner.Scan() {
		s := strings.Split(scanner.Text(), " ")
		i, e := strconv.Atoi(s[1])
        if e != nil {
            // handle error
            fmt.Println(e)
            os.Exit(2)
        }
		if s[0] == "up" {
			aim -= i
		} else if s[0] == "down" {
			aim += i
		} else if s[0] == "forward" {
			xpos += i
			depth += i * aim
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return xpos * depth
}

func day2() {

	testresult := day2b("./day2.test")
	expected := 900
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day2b("./day2.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

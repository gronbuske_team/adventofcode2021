package main

import (
	"strconv"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}


func getNum(s string) int {
	n, e := strconv.Atoi(s)
	if e != nil {
		fmt.Println(e)
		os.Exit(2)
	}
	return n
}

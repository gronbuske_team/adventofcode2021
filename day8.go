package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strings"
)

func day8a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	result := 0
	for scanner.Scan() {
		split := strings.Split(scanner.Text(), " | ")
		output := strings.Split(split[1], " ")
		for n := range output {
			if len(output[n]) == 2 || len(output[n]) == 3 || len(output[n]) == 4 || len(output[n]) == 7 {
				result++
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

func day8b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	result := 0
	for scanner.Scan() {
		split := strings.Split(scanner.Text(), " | ")
		output := strings.Split(split[1], " ")
		signal := strings.Split(split[0], " ")
		one := []byte{}
		seven := []byte{}
		four := []byte{}
		fives := []string{}
		sixes := []string{}

		for n := range signal {
			if len(signal[n]) == 2 {
				one = append(one, signal[n][0], signal[n][1])
			} else if len(signal[n]) == 3 {
				seven = append(seven, signal[n][0], signal[n][1], signal[n][2])
			} else if len(signal[n]) == 4 {
				four = append(four, signal[n][0], signal[n][1], signal[n][2], signal[n][3])
			} else if len(signal[n]) == 5 {
				fives = append(fives, signal[n])
			} else if len(signal[n]) == 6 {
				sixes = append(sixes, signal[n])
			}
		}
		topright := byte(' ')
		botright := byte(' ')
		for i := 0; i < 3; i++ {
			if topright != ' ' {
				break
			}
			rightlines := 0
			for j := 0; j < 6; j++ {
				if sixes[i][j] == one[0] || sixes[i][j] == one[1] {
					rightlines++
					botright = sixes[i][j]
				}
			}
			if rightlines == 1 {
				if botright == one[0] {
					topright = one[1]
				} else {
					topright = one[0]
				}
				break
			}
		}
		maybemid := []byte{}
		for i := 0; i < 4; i++ {
			if four[i] != topright && four[i] != botright {
				maybemid = append(maybemid, four[i])
			}
		}

		nums := []int{}
		for n := range output {
			if len(output[n]) == 2 {
				nums = append(nums, 1)
			} else if len(output[n]) == 3 {
				nums = append(nums, 7)
			} else if len(output[n]) == 4 {
				nums = append(nums, 4)
			} else if len(output[n]) == 5 {
				tr := false
				br := false
				for j := 0; j < 5; j++ {
					if output[n][j] == topright {
						tr = true
					}
					if output[n][j] == botright {
						br = true
					}
				}
				if tr && br {
					nums = append(nums, 3)
				} else if tr {
					nums = append(nums, 2)
				} else {
					nums = append(nums, 5)
				}
			} else if len(output[n]) == 6 {
				tr := false
				piece := 0
				for j := 0; j < 6; j++ {
					if output[n][j] == topright {
						tr = true
					}
					if output[n][j] == maybemid[0] || output[n][j] == maybemid[1] {
						piece++
					}
				}
				if !tr {
					nums = append(nums, 6)
				} else if piece == 2 {
					nums = append(nums, 9)
				} else {
					nums = append(nums, 0)
				}
			} else if len(output[n]) == 7 {
				nums = append(nums, 8)
			}
		}
		r := 0
		for n := range nums {
			r += nums[n] * int(math.Pow10(len(nums)-n-1))
		}
		result += r
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

func day8() {

	testresult := day8a("./day8.test")
	expected := 26
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day8a("./day8.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day8b("./day8.test")
	expected = 61229
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day8b("./day8.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

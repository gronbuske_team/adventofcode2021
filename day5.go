package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func day5a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	seafloor := [10000][10000]int{}
	result := 0
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), " -> ")
		s1 := strings.Split(s[0], ",")
		s2 := strings.Split(s[1], ",")
		x1 := float64(getNum(s1[0]))
		y1 := float64(getNum(s1[1]))
		x2 := float64(getNum(s2[0]))
		y2 := float64(getNum(s2[1]))
		if x1 == x2 {
			for y := int(math.Min(y1, y2)); y <= int(math.Max(y1, y2)); y++ {
				seafloor[int(x1)][y] += 1
				if seafloor[int(x1)][y] == 2 {
					result++
				}
			}
		} else if y1 == y2 {
			for x := int(math.Min(x1, x2)); x <= int(math.Max(x1, x2)); x++ {
				seafloor[x][int(y1)] += 1
				if seafloor[x][int(y1)] == 2 {
					result++
				}
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

func day5b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	seafloor := [10000][10000]int{}
	result := 0
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), " -> ")
		s1 := strings.Split(s[0], ",")
		s2 := strings.Split(s[1], ",")
		x1 := float64(getNum(s1[0]))
		y1 := float64(getNum(s1[1]))
		x2 := float64(getNum(s2[0]))
		y2 := float64(getNum(s2[1]))
		if x1 == x2 {
			for y := int(math.Min(y1, y2)); y <= int(math.Max(y1, y2)); y++ {
				seafloor[int(x1)][y] += 1
				if seafloor[int(x1)][y] == 2 {
					result++
				}
			}
		} else if y1 == y2 {
			for x := int(math.Min(x1, x2)); x <= int(math.Max(x1, x2)); x++ {
				seafloor[x][int(y1)] += 1
				if seafloor[x][int(y1)] == 2 {
					result++
				}
			}
		} else {
			length := int(math.Abs(x1 - x2))
			for l := 0; l <= length; l++ {
				x := 0
				if x1 > x2 { x = int(x1) - l } else { x = int(x1) + l }
				y := 0
				if y1 > y2 { y = int(y1) - l } else { y = int(y1) + l }
				seafloor[x][y] += 1
				if seafloor[x][y] == 2 {
					result++
				}
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	for i := 0; i < 10; i++ {
		s := ""
		for j := 0; j < 10; j++ {
			s += strconv.Itoa(seafloor[j][i])
		}
		fmt.Printf("%s\n", s)
	}
	return result
}

func day5() {

	testresult := day5a("./day5.test")
	expected := 5
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day5a("./day5.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day5b("./day5.test")
	expected = 12
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day5b("./day5.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

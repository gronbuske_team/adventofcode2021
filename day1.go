package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func day1a(inputfile string) int{
	// open file
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	// remember to close the file at the end of the program
	defer f.Close()

	// read the file line by line using scanner
	scanner := bufio.NewScanner(f)

	prev := 0
    larger := -1
    

	for scanner.Scan() {
		// do something with a line
		fmt.Printf("line: %s\n", scanner.Text())
		curr, err := strconv.Atoi(scanner.Text())
        if err != nil {
            // handle error
            fmt.Println(err)
            os.Exit(2)
        }
        if curr > prev{
            larger += 1
        }
        prev = curr
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
    return larger
}

func day1b(inputfile string) int{
	// open file
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	// remember to close the file at the end of the program
	defer f.Close()

	// read the file line by line using scanner
	scanner := bufio.NewScanner(f)

	prev := [3]int{0, 0, 0}
    larger := -3
    
	for scanner.Scan() {
		// do something with a line
		fmt.Printf("line: %s\n", scanner.Text())
		curr, err := strconv.Atoi(scanner.Text())
        if err != nil {
            // handle error
            fmt.Println(err)
            os.Exit(2)
        }
        if curr > prev[0] {
            larger += 1
        }
        prev[0] = prev[1]
        prev[1] = prev[2]
        prev[2] = curr
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
    return larger
}

func day1() {

    testresult := day1a("./day1.test")
    expected := 7
    if testresult == expected {
        fmt.Printf("Test passed: %d\n", testresult)
        result := day1a("./day1.input")
        fmt.Printf("Result: %d\n", result)
    } else {
        fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
    }

    testresult = day1b("./day1.test")
    expected = 5
    if testresult == expected {
        fmt.Printf("Test passed: %d\n", testresult)
        result := day1b("./day1.input")
        fmt.Printf("Result: %d\n", result)
    } else {
        fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
    }
}

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

func day10a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	result := 0
	for scanner.Scan() {
		text := scanner.Text()
		stack := []byte{}
		for n := range text {
			if text[n] == '(' || text[n] == '<' || text[n] == '{' || text[n] == '[' {
				stack = append(stack, text[n])
			} else {
				l := stack[len(stack)-1]
				stack = stack[:len(stack)-1]
				if text[n] == ')' {
					if l != '(' {
						result += 3
						break
					}
				} else if text[n] == '>' {
					if l != '<' {
						result += 25137
						break
					}
				} else if text[n] == '}' {
					if l != '{' {
						result += 1197
						break
					}
				} else if text[n] == ']' {
					if l != '[' {
						result += 57
						break
					}
				}
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

func day10b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	results := []int{}
	for scanner.Scan() {
		text := scanner.Text()
		stack := []byte{}
		correct := true
		for n := range text {
			if text[n] == '(' || text[n] == '<' || text[n] == '{' || text[n] == '[' {
				stack = append(stack, text[n])
			} else {
				l := stack[len(stack)-1]
				stack = stack[:len(stack)-1]
				if text[n] == ')' {
					if l != '(' {
						correct = false
						break
					}
				} else if text[n] == '>' {
					if l != '<' {
						correct = false
						break
					}
				} else if text[n] == '}' {
					if l != '{' {
						correct = false
						break
					}
				} else if text[n] == ']' {
					if l != '[' {
						correct = false
						break
					}
				}
			}
		}
		if !correct {continue}
		r := 0
		for s := range stack {
			if stack[len(stack) - 1 - s] == '(' {
				r = r * 5 + 1
			} else if stack[len(stack) - 1 - s] == '[' {
				r = r * 5 + 2
			} else if stack[len(stack) - 1 - s] == '{' {
				r = r * 5 + 3
			} else if stack[len(stack) - 1 - s] == '<' {
				r = r * 5 + 4
			}
		}
		results = append(results, r)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	sort.Ints(results)
	return results[len(results) / 2]
}

func day10() {

	testresult := day10a("./day10.test")
	expected := 26397
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day10a("./day10.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day10b("./day10.test")
	expected = 288957
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day10b("./day10.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

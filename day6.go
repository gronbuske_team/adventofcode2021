package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func day6a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	fish := [9]int{}
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), ",")
		for n := range s {
			t := getNum(s[n])
			fish[t]++
		}
		for i := 0; i < 80; i++ {
			t1 := fish[0]
			for x := 8; x >= 0; x-- {
				t2 := fish[x]
				fish[x] = t1
				t1 = t2
				if x == 6 {
					fish[x] += fish[0]
				}
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	result := 0
	for x := 0; x < 9; x++ {
		result += fish[x]
	}
	return result
}

func day6b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	fish := [9]int{}
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), ",")
		for n := range s {
			t := getNum(s[n])
			fish[t]++
		}
		for i := 0; i < 256; i++ {
			t1 := fish[0]
			for x := 8; x >= 0; x-- {
				t2 := fish[x]
				fish[x] = t1
				t1 = t2
				if x == 6 {
					fish[x] += fish[0]
				}
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	result := 0
	for x := 0; x < 9; x++ {
		result += fish[x]
	}
	return result
}

func day6() {

	testresult := day6a("./day6.test")
	expected := 5934
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day6a("./day6.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day6b("./day6.test")
	expected = 26984457539
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day6b("./day6.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

var octopusGrid [][]int
var flashes int

func flash(x int, y int) {
	if x < 0 || y < 0 || x > 9 || y > 9 {
		return
	}
	if octopusGrid[x][y] == 0 {
		return
	}
	octopusGrid[x][y]++
	checkOctopus(x, y)
}

func checkOctopus(x int, y int) {
	if x < 0 || y < 0 || x > 9 || y > 9 {
		return
	}
	if octopusGrid[x][y] > 9 {
		flashes++
		octopusGrid[x][y] = 0
		flash(x, y+1)
		flash(x, y-1)
		flash(x+1, y)
		flash(x-1, y)
		flash(x+1, y+1)
		flash(x+1, y-1)
		flash(x-1, y+1)
		flash(x-1, y-1)
	}
}

func day11a(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	octopusGrid = [][]int{}
	c := 0
	flashes = 0
	for scanner.Scan() {
		octopusGrid = append(octopusGrid, []int{})
		text := scanner.Text()
		for n := range text {
			octopusGrid[c] = append(octopusGrid[c], getNum(string(text[n])))
		}
		c++
	}
	for t := 0; t < 100; t++ {
		for x := 0; x < 10; x++ {
			for y := 0; y < 10; y++ {
				octopusGrid[x][y]++
			}
		}
		for x := 0; x < 10; x++ {
			for y := 0; y < 10; y++ {
				checkOctopus(x, y)
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return flashes
}

func day11b(inputfile string) int {
	f, err := os.Open(inputfile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	octopusGrid = [][]int{}
	c := 0
	flashes = 0
	for scanner.Scan() {
		octopusGrid = append(octopusGrid, []int{})
		text := scanner.Text()
		for n := range text {
			octopusGrid[c] = append(octopusGrid[c], getNum(string(text[n])))
		}
		c++
	}
	c = 0
	for {
		c++
		for x := 0; x < 10; x++ {
			for y := 0; y < 10; y++ {
				octopusGrid[x][y]++
			}
		}
		before := flashes
		for x := 0; x < 10; x++ {
			for y := 0; y < 10; y++ {
				checkOctopus(x, y)
			}
		}
		if flashes-before == 100 {
			return c
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return 0
}

func day11() {

	testresult := day11a("./day11.test")
	expected := 1656
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day11a("./day11.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}

	testresult = day11b("./day11.test")
	expected = 195
	if testresult == expected {
		fmt.Printf("Test passed: %d\n", testresult)
		result := day11b("./day11.input")
		fmt.Printf("Result: %d\n", result)
	} else {
		fmt.Printf("Test failed got %d, expected %d\n", testresult, expected)
	}
}
